package cus1156.catlab2;

import java.util.ArrayList;

/**
 * This class represents a group of cats. The cats are created when the CatManager is created
 * although more cats can be added later.
 * 
 */
public class CatManager
{
	private ArrayList<Cat> myCats = new ArrayList<Cat>();

	public CatManager()
	{
		Cat cat = new Cat("Fifi", "black");
		myCats.add(cat);
		cat = new Cat("Fluffy", "spotted");
		myCats.add(cat);
		cat = new Cat("Josephine", "tabby");
		myCats.add(cat);
		cat = new Cat("Biff", "tabby");
		myCats.add(cat);
		cat = new Cat("Bumpkin", "white");
		myCats.add(cat);
		cat = new Cat("Spot", "spotted");
		myCats.add(cat);
		cat = new Cat("Lulu", "tabby");
		myCats.add(cat);
	}

	/** add a cat to the group
	 * 
	 * @param name
	 * @return
	 */
	public void add(Cat aCat)
	{
		myCats.add(aCat);
	}
	


	/**returns the first cat whose name matches, or null if no cat
                    // with that name is found
	 * 
	 * @param name - the name of the Cat that we are searching for
	 * @return the Cat object or null if not found
	 */ 
	
	public Cat findThisCat(String name)
	{
		for (int i = 0; i < myCats.size(); i++) {
			if (name.equalsIgnoreCase(myCats.get(i).getName())) {
				return myCats.get(i);
			}
		}
		return null;
	}

	
	
	/**
	 * returns the number of cats of this color
	 * @param color - the color we are counting
	 * @return - the number of cats
	 */
	
	
	
	public int countColors(String color)
	{  
		int totCats = 0;
		for (int i = 0; i < myCats.size(); i++) {
			if (color.equalsIgnoreCase(myCats.get(i).getColor())) {
				totCats++;
			}
		}
		return totCats;
	}
	
}
