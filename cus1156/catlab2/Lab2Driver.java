package cus1156.catlab2;

import java.util.Scanner;


public class Lab2Driver
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		CatManager catMgr = new CatManager();

		// This code finds the number of Tabby and Spotted Cats		
		int numTabby = catMgr.countColors("tabby");
		int numSpotted = catMgr.countColors("spotted");
		System.out.println("There are " + numTabby + " tabby cats");
		System.out.println("There are " + numSpotted + " spotted cats\n\n");
		
		// This code adds a new cat to your group of Cats.
		System.out.println("Enter the name of the cat you would like to register:");
		String newCatName = input.next();
		System.out.println("Enter the color/pattern of the cat you would like to register:");
		String newCatColor = input.next();
		Cat newCat = new Cat(newCatName, newCatColor);
		catMgr.add(newCat);
		System.out.println("Congratulations! Your cat has been registered.\n\n");
		
		// This code finds the cat requested by user.
		System.out.println("Enter the name of the cat you would like to find:");
		String name =  input.next();
		Cat foundCat = catMgr.findThisCat(name);
		if (foundCat == null)
			System.out.println("Sorry! We could not find this cat...");
		else
			foundCat.manyMeows(3);
		
		input.close();
	}

}
